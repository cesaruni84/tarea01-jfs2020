/**
 * 
 */
package com.mitocode.controller;

import java.net.URI;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.CustomException;
import com.mitocode.model.Persona;
import com.mitocode.service.IPersonaService;

/**
 * @author CESAR
 *
 */

@RestController
@RequestMapping("/personas")
public class PersonaController {
	
	
	@Autowired
	private IPersonaService personaService;
	
	@GetMapping
	public ResponseEntity<List<Persona>> listar(){
		List<Persona> lista = personaService.listar();
		return new ResponseEntity<List<Persona>>(lista, HttpStatus.OK);
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<?> listarPorId(@PathVariable("id") Integer id){
		Persona p = personaService.listarPorId(id);
		if(p.getIdPersona() == null) {
			throw new CustomException ("Id de persona no encontrado: " + id);
		}
		return new ResponseEntity<Persona>(p, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<?> registrar(@Valid @RequestBody Persona persona){
		Persona p = personaService.registrar(persona);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(p.getIdPersona()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<Persona> modificar(@Valid @RequestBody Persona persona){
		Persona p = personaService.modificar(persona);
		return new ResponseEntity<Persona>(p, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminar(@PathVariable("id") Integer id) {
		Persona p = personaService.listarPorId(id);
		if(p.getIdPersona() == null) {
			throw new CustomException("Id de persona no encontrado: " + id);
		}
		personaService.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}

}
