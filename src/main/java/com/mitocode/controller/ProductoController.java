/**
 * 
 */
package com.mitocode.controller;

import java.net.URI;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.mitocode.exception.CustomException;
import com.mitocode.model.Producto;
import com.mitocode.service.IProductoService;

/**
 * @author CESAR
 *
 */

@RestController
@RequestMapping("/productos")
public class ProductoController {
	
	@Autowired
	private IProductoService productoService;
	
	@GetMapping
	public ResponseEntity<List<Producto>> listarProductos(){
		List<Producto> lista = productoService.listar();
		return new ResponseEntity<List<Producto>>(lista, HttpStatus.OK);
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<?> listarProductoPorId(@PathVariable("id") Integer id){
		Producto p = productoService.listarPorId(id);
		if(p.getIdProducto() == null) {
			throw new CustomException ("Id de producto no encontrado: " + id);
		}
		return new ResponseEntity<Producto>(p, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<?> registrarProducto(@Valid @RequestBody Producto producto){
		Producto p = productoService.registrar(producto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(p.getIdProducto()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<Producto> modificarProducto(@Valid @RequestBody Producto producto){
		Producto p = productoService.modificar(producto);
		return new ResponseEntity<Producto>(p, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarProductoPorId(@PathVariable("id") Integer id) {
		Producto p = productoService.listarPorId(id);
		if(p.getIdProducto() == null) {
			throw new CustomException("Id de producto no encontrado: " + id);
		}
		productoService.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}


}
