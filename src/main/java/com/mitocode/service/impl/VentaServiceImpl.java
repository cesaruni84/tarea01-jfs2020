/**
 * 
 */
package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mitocode.model.Venta;
import com.mitocode.repository.IVentaRepository;
import com.mitocode.service.IVentaService;

/**
 * @author CESAR
 *
 */

@Service
public class VentaServiceImpl implements IVentaService {
	
	@Autowired
	private IVentaRepository ventaRepository;
	
	@Transactional
	@Override
	public Venta registrarVenta(Venta ventaDTO) {
		ventaDTO.getDetalleVenta().forEach(detalleVenta -> {
			detalleVenta.setVenta(ventaDTO);
		});
		return ventaRepository.save(ventaDTO);
	}
}
