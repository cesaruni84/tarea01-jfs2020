/**
 * 
 */
package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mitocode.model.Persona;
import com.mitocode.repository.IPersonaRepository;
import com.mitocode.service.IPersonaService;

/**
 * @author CESAR
 *
 *
 */

@Service
public class PersonaServiceImpl implements IPersonaService {
	
	@Autowired
	private IPersonaRepository personaRepository;

	@Override
	public Persona registrar(Persona obj) {
		return personaRepository.save(obj);
	}

	@Override
	public Persona modificar(Persona obj) {
		return personaRepository.save(obj);
	}

	@Override
	public List<Persona> listar() {
		return personaRepository.findAll();
	}

	@Override
	public Persona listarPorId(Integer id) {
		Optional<Persona> persona = personaRepository.findById(id);
		return persona.isPresent() ? persona.get() : new Persona();	
	}

	@Override
	public boolean eliminar(Integer id) {
		personaRepository.deleteById(id);
		return true;
	}

}
