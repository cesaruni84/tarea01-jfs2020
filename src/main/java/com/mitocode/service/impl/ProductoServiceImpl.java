/**
 * 
 */
package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mitocode.model.Producto;
import com.mitocode.repository.IProductoRepository;
import com.mitocode.service.IProductoService;

/**
 * @author CESAR
 *
 */

@Service
public class ProductoServiceImpl implements IProductoService {
	
	
	@Autowired
	private IProductoRepository productoRepository;

	@Override
	public Producto registrar(Producto obj) {
		return productoRepository.save(obj);
	}

	@Override
	public Producto modificar(Producto obj) {
		return productoRepository.save(obj);
	}

	@Override
	public List<Producto> listar() {
		return productoRepository.findAll();
	}

	@Override
	public Producto listarPorId(Integer id) {
		Optional<Producto> producto = productoRepository.findById(id);
		return producto.isPresent() ? producto.get() : new Producto();	
	}

	@Override
	public boolean eliminar(Integer id) {
		productoRepository.deleteById(id);
		return true;
	}

}
