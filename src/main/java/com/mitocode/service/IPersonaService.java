package com.mitocode.service;

import com.mitocode.model.Persona;

/**
 * @author CESAR
 *
 */
public interface IPersonaService extends ICRUD<Persona, Integer> {

}
