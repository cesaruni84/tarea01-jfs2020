/**
 * 
 */
package com.mitocode.service;

import com.mitocode.model.Venta;

/**
 * @author CESAR
 *
 */
public interface IVentaService {
	
	Venta registrarVenta(Venta venta);

}
