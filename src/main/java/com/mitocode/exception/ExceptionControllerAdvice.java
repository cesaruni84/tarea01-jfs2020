/**
 * 
 */
package com.mitocode.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author CESAR
 *
 */

@ControllerAdvice
@RestController
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(),
				"Error inesperado, contacte al administrador");
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}
	

	@ExceptionHandler(CustomException.class)
	public final ResponseEntity<ErrorResponse> exceptionHandlerCustom(CustomException ex){
		ErrorResponse er = new ErrorResponse("ERR-903", ex.getMessage());
		return new ResponseEntity<ErrorResponse>(er, HttpStatus.NOT_FOUND);
	}

}
